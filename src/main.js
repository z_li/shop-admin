import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import '@/assets/reset.css'
import 'reset-css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import {http} from '@/http'
import MyPlugin from './plugins'
console.log(MyPlugin)
Vue.use(ElementUI)
Vue.use(MyPlugin)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
