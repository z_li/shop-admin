import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token:localStorage.getItem('token')||'',
    username:localStorage.getItem('username')||''
  },
  mutations: {
    setToke(state,data){
      state.token = data
      localStorage.setItem('token',data)
    },
    setUsername(state,data){
      state.username = data
      localStorage.setItem('username',data)
    },
  },
  actions: {
  },
  modules: {
  }
})
