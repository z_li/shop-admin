import {http} from '@/http'
const MyPlugin = {}
MyPlugin.install = (Vue, options) => {
    Vue.prototype.http = (...obj) => {
        return http(...obj)
    }
}

export default MyPlugin