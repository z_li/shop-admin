import axios from 'axios'
import {Message} from 'element-ui'
axios.defaults.baseURL = 'https://pc.raz-kid.cn/api/private/v1/'
export const http =  (url, method='get', data={}) => {
    return new Promise((resolve, reject)=>{
        axios({
           url,
           method: method.toLowerCase(),
           data  
        }).then(res => {
            if (res.status >= 200 && res.status < 300 || res.status===304) {
                if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
                    resolve(res.data)
                }else {
                    Message.error(res.data.meta.msg);
                    reject(res)
                }
            }else {
                Message.error(res.statusText);
                reject(res)
            }
        }).catch(err => {
            reject(err)
        })
    })
}